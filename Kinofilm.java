import java.time.*;
/**
 * Ein Kinofilm.
 * 
 * @author S.Gebert 
 * @version 12.2020
 */
public class Kinofilm extends Film
{
    /**
     * Konstruktor für Objekte der Klasse Kinofilm
     *
     * @param titel Filmtitel
     * @param produktionsjahr Das Produktionsjahr
     * @param genre Hauptgenre
     * @param laufzeit Laufzeit in Minuten
     * @param altersfreigabe Altersfreigabe in Jahren
     * @param bewertung Bewertung (0.0 bis 10.0)
     */
    public Kinofilm(String titel, Year produktionsjahr, String genre, int laufzeit, int altersfreigabe, double bewertung)
    {
        super(titel, produktionsjahr, genre, laufzeit, altersfreigabe, bewertung);
        this.typ = FilmTyp.KINOFILM;
    }

    /**
     * Konstruktor für Objekte der Klasse Kinofilm
     *
     * @param titel Filmtitel
     * @param produktionsjahr Das Produktionsjahr
     * @param genre Hauptgenre
     * @param laufzeit Laufzeit in Minuten
     * @param altersfreigabe Altersfreigabe in Jahren
     */
    public Kinofilm(String titel, Year produktionsjahr, String genre, int laufzeit, int altersfreigabe)
    {
        super(titel, produktionsjahr, genre, laufzeit, altersfreigabe);
        this.typ = FilmTyp.KINOFILM;
    }
}
