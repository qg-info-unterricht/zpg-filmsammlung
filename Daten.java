import java.util.*;
import java.time.*;

/**
 * Stellt statische Methoden zur Verfügung, welche Testdaten zur Nutzung im Projekt bereitstellen.
 * 
 * @author S.Gebert
 * @version 12.2020
 */
public class Daten
{
    static HashMap<String,Kinofilm> kinofilme()
    {
        HashMap<String, Kinofilm> filme = new HashMap<>();

        filme.put("f1", new Kinofilm( "The Terminator", Year.of(1984), "Sci-Fi", 107, 16, 8.0 ));
        filme.put("f2", new Kinofilm( "Terminator 2", Year.of(1991), "Sci-Fi", 137, 16, 8.5 ));
        filme.put("f3", new Kinofilm( "Terminator 3", Year.of(2003), "Sci-Fi", 109, 16, 6.3 ));
        filme.put("f4", new Kinofilm( "Terminator Salvation", Year.of(2009), "Sci-Fi", 115, 16, 6.5 ));
        filme.put("f5", new Kinofilm( "Terminator Genisys", Year.of(2015), "Sci-Fi", 126, 12, 6.3 ));
        filme.put("f6", new Kinofilm( "Terminator: Dark Fate", Year.of(2019), "Sci-Fi", 128, 16, 6.2 ));
        filme.put("f7", new Kinofilm( "Forest Gump", Year.of(2019), "Satire", 142, 12, 8.2));
        filme.put("f8", new Kinofilm( "Die Känguru-Chroniken", Year.of(2020), "Komödie", 92, 0, 5.4 ));

        return filme;
    }

    static HashMap<String,Serie> serien()
    {
        HashMap<String, Serie> filme = new HashMap<>();
        Serie s1 =   new Serie( "Terminator: The Sarah Connor Chronicles", Year.of(2008), "Sci-Fi", 16 );
        filme.put("s1", s1);
        SerienEpisode e1 = new SerienEpisode( "Pilot", null, "Drama", 46, 7.8, s1, 1, 1 );
        SerienEpisode e2 = new SerienEpisode( "Dungeons & Dragons", null, "Action", 44, 7.0, s1, 1, 6 );
        Serie s2 = new Serie( "The Big Bang Theory", Year.of(2007), "Komödie", 6 );
        filme.put("s2", s2);
        SerienEpisode e3 = new SerienEpisode( "The Middle Earth Paradigm", Year.of(2009), "Komödie", 21, 8.5, s2, 1, 6 );
        SerienEpisode e4 = new SerienEpisode( "The Stockholm Syndrome", Year.of(2019), "Komödie", 23, 9.6, s2, 12, 24 );

        return filme;
    }

    static Serie randomSerie()
    {
        return serien().values().stream().skip( new Random().nextInt(serien().size())).findFirst().get();
    }

    static Kinofilm randomKinofilm()
    {
        return kinofilme().values().stream().skip( new Random().nextInt(serien().size())).findFirst().get();
    }

    static Film randomFilm()
    {
        if( Math.random() > 0.5 ) return randomSerie();
        return randomKinofilm();              
    }

    static SerienEpisode randomEpisode()
    {
        Serie e = randomSerie();
        return (SerienEpisode) e.getEpisoden().get( (int)Math.round((e.getAnzahlEpisoden()-1) * Math.random()));     
    }
}
