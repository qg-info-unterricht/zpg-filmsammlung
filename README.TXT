PROJEKTBEZEICHNUNG: Filmsammlung
PROJEKTZWECK: Einführung in generische Datentypen, Sammlungen und Iteration über Elemente einer Sammlung z.B.über die erweiterte For-Schleife und Lambdaausdrücke
VERSION oder DATUM: 02.2021
WIE IST DAS PROJEKT ZU STARTEN: Das Projekt wird über die Direkteingabe in BlueJ gesteuert.
AUTOR(EN): S.Gebert
BENUTZERHINWEISE: Mit der Unit-Test Klasse "Basis" kann ein Objektzustand als Ausgangsbasis vorgegeben werden. Auf dieser wird dann über die Direkteingabe operiert.
 In der Klasse "Aufgaben" werden statische Methoden pro Aufgabe definiert, welche über die Direkteingabe aufgerufen werden.
 Beispielaufgaben finden sich im Begleitmaterial.
