

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Ausgangsbasis für Operationen in der Direkteigabe.
 *
 * @author  S.Gebert
 * @version 12.2020
 */
public class Basis
{
    Filmsammlung f;
    Filmsammlung kf;
    Filmsammlung s; 
    FilmsammlungGeneric<Kinofilm> kfG;
    FilmsammlungGeneric<Serie> sG;
    
    /**
     * Konstruktor fuer die Test-Klasse ProjektTest
     */
    public Basis()
    {
    }

    /**
     *  Setzt das Testgerüst fuer den Test.
     *
     * Wird vor jeder Testfall-Methode aufgerufen.
     */
    @Before
    public void setUp()
    {
        kf = new Filmsammlung( Daten.kinofilme().values() );
        s = new Filmsammlung( Daten.serien().values() );
        kfG = new FilmsammlungGeneric<Kinofilm>( Daten.kinofilme().values() );
        sG = new FilmsammlungGeneric<Serie>( Daten.serien().values() );
    }

    /**
     * Gibt das Testgerüst wieder frei.
     *
     * Wird nach jeder Testfall-Methode aufgerufen.
     */
    @After
    public void tearDown()
    {
    }
}
