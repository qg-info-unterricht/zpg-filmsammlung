import java.util.*;
import java.time.*;

/**
 * Eine Episode einer Serie
 * 
 * @author S.Gebert 
 * @version 12.2020
 */
public class SerienEpisode extends Film
{
    /**
     * Die Serie zu der die Episode gehört
     */
    private Serie serie;
    /**
     * Die Nummer der Episode innerhalb der Serie
     */
    private int episode;
    /**
     * Die Nummer der Staffel zu der die Episode gehört
     */
    private int staffel;

    /**
     * Erzeugt eine Episode einer Serie
     *
     * @param titel Filmtitel
     * @param produktionsjahr Das Produktionsjahr
     * @param genre Hauptgenre
     * @param laufzeit Laufzeit in Minuten
     * @param bewertung Bewertung (0.0 bis 10.0)
     * @param serie Zugehörige Serie
     * @param staffel Nummer der Staffel
     * @param episode Nummer der Episode
     */
    public SerienEpisode(String titel, Year produktionsjahr, String genre, int laufzeit, double bewertung, Serie serie, int staffel, int episode )
    {
        super(titel, produktionsjahr, genre, laufzeit, -1, bewertung);
        this.typ = FilmTyp.EPISODE;
        this.serie = serie;
        this.serie.addEpisode(this);
        this.staffel = staffel;
        this.episode = episode;
    }

    /**
     * Erzeugt eine Episode einer Serie
     *
     * @param titel Filmtitel
     * @param produktionsjahr Das Produktionsjahr
     * @param genre Hauptgenre
     * @param laufzeit Laufzeit in Minuten
     * @param serie Zugehörige Serie
     * @param staffel Nummer der Staffel
     * @param episode Nummer der Episode
     */
    public SerienEpisode(String titel, Year produktionsjahr, String genre, int laufzeit, Serie serie, int staffel, int episode )
    {
        super(titel, produktionsjahr, genre, laufzeit, -1);
        this.typ = FilmTyp.EPISODE;
        this.serie = serie;
        this.staffel = staffel;
        this.episode = episode;
    }

    /* TODO: Getter
     * Schreibe Getter für die privaten Attribute und dokumentiere diese.
     */
}
