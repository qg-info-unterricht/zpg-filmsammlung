import java.util.*;
import java.time.*;

/**
 * Verschiedene Aufgaben, die statisch direkt aus BlueJ heraus aufgerufen werden.
 * 
 * @author S.Gebert 
 * @version 12.2020
 */
public class Aufgabe
{

    /**
     * Aufgabe.a1
     * Schreibe eine Methode, die für das erste Element einer gegebene Filmsammlung (von Serien) die Bewertung auf 5.0 setzt
     * und
     * den Titel und die Anzahl der vorhandenen Episoden auf der Konsole ausgibt.
     * 
     * Hinweise:    - Betrachte die Dokumentation der Klasse Filmsammlung. Mit welcher Methode kannst du auf die Elemente der Sammlung zugreifen?
     *              - Prüfe an welcher Stelle eine Typkonvertierung nötig ist.
     * 
     * Teste deine Methode mit der Filmsammlung s und kf (Basis - Objektzustand wiederherstellen)
     *
     * @param fs Eine Filmsammlung
     */
    public static void a1(Filmsammlung fs)
    {
        
    }

    /**
     * Aufgabe.a2
     * Schreibe eine Methode, die für das zweite Element einer gegebene Filmsammlung (von Serien) die Bewertung auf 5.0 setzt
     * und
     * den Titel und die Anzahl der vorhandenen Episoden auf der Konsole ausgibt.
     * 
     * Teste deine Methode mit der Filmsammlung s und kf (Basis - Objektzustand wiederherstellen)
     * 
     * @param fsG Eine Seriensammlung
     */
    public static void a2(FilmsammlungGeneric<Serie> fsG)
    {
       
    }

    /**
     * Aufgabe.a3
     * Erstelle verschieden Filmsammlungen und teste deren Methoden.
     * Um einzelne Filme zu erhalten kannst die Methoden Daten.randomFilm(), Daten.randomKinofilm(), Daten.randomSerie() und Daten.randomEpisode() verwenden.
     * 
     * - Welche konkreten Typparameter sind erlaubt, welche nicht?
     * - Finde Beispiele für die es zu Compilierungs/Laufzeitfehlern kommen kann.
     */
    public static void a3()
    {
        //Beispiel
        FilmsammlungGeneric<SerienEpisode> sammlung1 = new FilmsammlungGeneric<>( new SerienEpisode[]{Daten.randomEpisode(), Daten.randomEpisode() });
        System.out.println(sammlung1.get(0).getTitel());
        
        //Weitere Beispiele

    }

    /**
     * Aufgabe.a4
     * Die gegebene Methode soll die Titel aller Filme in einer Filmsammlung ausgeben.
     * Teste die gegebene Methode mit den Basisdaten sG und kfG.
     * 
     *  - Notiere dir den Laufzeitfehler den du erhälst.
     *  - Ändere die Methode so ab, dass es mit sG bzw. kfG funktioniert. Gibt es eine allgemeine Lösung?
     * @param fsG Eine Filmsammlung
     */
    public static void a4(FilmsammlungGeneric<Film> fsG)
    {
        Iterator<Film> iter = fsG.iterator();

        while( iter.hasNext() ){
            System.out.print( iter.next().getTitel() +", ");
        }

    }

    /**
     * Aufgabe.a5
     * Die gegebene Methode soll die Titel aller Filme in einer Filmsammlung von Kinofilmen ausgeben.
     * Implementiere die Methode mit der erweiterten for-Schleife
     * Teste mit den Basisdaten kfG.
     *
     * @param fsG Eine Filmsammlung
     */
    public static void a5(FilmsammlungGeneric<Kinofilm> fsG)
    {
      
    }

    /**
     * Aufgabe.a6
     * Finde für jede gegebene Reihung jeweils das Minimum.
     * Schreibe dafür je eine private statische Methode und rufe diese auf.
     * Gib das Ergebnis auf der Konsole aus.
     * 
     */
    public static void a6()
    {
        //Gegebene Reihungen
        Integer[] rInt = {5, 90, 4, 1, -70, 400, 30};
        String[] rString = {"Bär","Affe","Nilpferd","Zebra"};
        Float[] rFloat = {3.4f, 9.78f,.01f};

        //Aufruf

    }
}
