
/**
 * Globale Einstellungen des Projekts
 * 
 * @author S.Gebert 
 * @version 12.2020
 */
public class Settings
{
    /**
     * Verbosität
     * {@link #NONE}
     * {@link #INFO}
     * {@link #DEBUG}
     * {@link #ALL}
     */
    static enum V_LEVEL {
        /**
         * Keine Ausgaben in der Konsole
         */
        NONE,
        /**
         * Auf der Konsole werden Informationen für den Nutzer ausgegeben.
         */
        INFO,
        /**
         * Auf der Konsole werden Informationen für den Programmierer ausgegeben.
         */
        DEBUG,
        /**
         * Alle verfügbaren Informationen werden auf der Konsole ausgegeben.
         */
        ALL    
    }
    
    /**
     * Die Verbosität des Programms.
     * Im Code kann diese überprüft und entsprechend Ausgaben erstellt werden.
     */
    static V_LEVEL VERBOSE = V_LEVEL.INFO;  
}
