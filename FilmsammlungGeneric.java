import java.util.*;

/**
 * Eine Filmsammlung.
 * Generische Implementierung auf Basis einer ArrayList<E>
 * 
 * @author S.Gebert 
 * @version 12.2020
 */
public class FilmsammlungGeneric<E> extends ArrayList<E>
//public class FilmsammlungGeneric<E extends Film> extends ArrayList<E>
{

    /**
     * Erstellt eine leere Filmsammlung.
     *
     * @param laenge Die Anzahl Filme, welche zu Beginn in die Filmsammlung passen.
     */
    public FilmsammlungGeneric(int laenge)
    {
        super(laenge);
    }

    /**
     * Erstellt eine Filmsammlung und füllt diese mit gegebenen Filmen.
     *
     * @param sammlung Ein Reihung aller Filme welche in die Filmsammlung aufgenommen werden sollen.
     */
    public FilmsammlungGeneric(E[] sammlung)
    {
        super( Arrays.asList(sammlung) );
    }

    /**
     * Erstellt eine Filmsammlung und füllt diese mit gegebenen Filmen.
     *
     * @param sammlung Ein Sammlung aller Filme welche in die Filmsammlung aufgenommen werden sollen.
     */
    public FilmsammlungGeneric(Collection<? extends E> c)
    {
        super(c);
    }

    /**
     * Sucht einen Film anhand dessen Titels und gibt die Position in der Sammlung zurück.
     * Konnte der Film nicht gefunden werden, wird -1 zurück gegeben.
     *
     * @param titel Der Titel des gesuchten Films
     * @return Die Position des gesuchten Films
     */
    public int find( String titel )
    {
        /* TODO: Suche
         * Implementiere eine einfache sequentielle bzw. lineare Suche
         * z.B. mit einem Lambdaausdruck
         */
        return 0; 
    }

    /**
     * Gibt die Anzahl Filme in der Sammlung zurück
     *
     * @return Die Anzahl Filme in der Sammlung
     */
    public int anzahlFilme()
    {
        return this.size();
    }

    
    /**
     * Bestimmt die durchschnittliche Bewertung aller Filme aus der Sammlung.
     *
     * @return Die durchschnittle Bewertung aller Filme aus der Sammlung
     */
    // public double getBewertung( )
    // {
        // double sum = 0.0;
        // int num = 0;

        // /* TODO: Alternative Implementierung
         // * Nutze statt der erweiterten For-Schleife einen Lambdaausdruck.
         // */

        // for( E e: this ){
            // if( e != null ){
                // sum += e.getBewertung();
                // num ++;
            // }
        // }

        // return Math.round(sum/num * 10.0) / 10.0; //auf eine Nachkommastelle runden.
    // }
}
