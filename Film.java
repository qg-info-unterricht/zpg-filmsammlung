    import java.time.*;

/**
 * Ein Film, wie er z.B. in der www.imdb.com oder moviepilot.de eingetragen sein könnte.
 * 
 * @author S.Gebert 
 * @version 12.2020
 */
public abstract class Film
{
    /**
     * Mögliche Filmtypen
     * {@link #KINOFILM}
     * {@link #SERIE}
     * {@link #EPISODE}
     */
    public static enum FilmTyp {
        /**
         * Ein Kinofilm
         */
        KINOFILM,
        /**
         * Eine Serie
         */
        SERIE,
        /**
         * Eine Episode einer Serie
         */
        EPISODE
    }

    /**
     * Der Filmtyp des Films
     */
    protected FilmTyp typ;

    /**
     * Der Filmtitel
     */
    private String titel;
    /**
     * Das Produktionsjahr
     */
    private Year produktionsjahr; // Year implementiert Comparable<Year>
    /**
     * Das Genre. Bei mehreren möglichen Genre, dasjenige in das der Film am besten passt.
     */
    private String genre;
    /**
     * Die Laufzeit in Minuten
     */
    private int laufzeit;
    /**
     * Die Altersfreigabe in Jahren
     */
    private int altersfreigabe;
    /**
     * Eine Bewertung. Von unterirdisch 0.0 bis 10.0 göttlich
     */
    private double bewertung; // von 0.0 bis 10.0

    /**
     * Erzeugt einen Film
     *
     * @param titel Filmtitel
     * @param produktionsjahr Das Produktionsjahr
     * @param genre Hauptgenre
     * @param laufzeit Laufzeit in Minuten
     * @param altersfreigabe Altersfreigabe in Jahren
     */
    public Film(String titel, Year produktionsjahr, String genre, int laufzeit, int altersfreigabe)
    {
        this.titel = titel;
        this.produktionsjahr = produktionsjahr;
        this.genre = genre;
        this.laufzeit = laufzeit;
        this.altersfreigabe = altersfreigabe;
    }

    /**
     * Erzeugt einen Film
     *
     * @param titel Filmtitel
     * @param produktionsjahr Das Produktionsjahr
     * @param genre Hauptgenre
     * @param laufzeit Laufzeit in Minuten
     * @param altersfreigabe Altersfreigabe in Jahren
     * @param bewertung Bewertung (0.0 bis 10.0)
     */
    public Film(String titel, Year produktionsjahr, String genre, int laufzeit, int altersfreigabe, double bewertung)
    {
        this.titel = titel;
        this.produktionsjahr = produktionsjahr;
        this.genre = genre;
        this.laufzeit = laufzeit;
        this.altersfreigabe = altersfreigabe;
        this.bewertung = bewertung;
    }

    /**
     * Setter für Bewertung
     *
     * @param bewertung Bewertung (0.0 bis 10.0)
     */
    public void setBewertung( double bewertung )
    {
        if( this.bewertung == bewertung ) return;
        if( Settings.VERBOSE == Settings.V_LEVEL.INFO ) System.out.println("Bisherige Bewertung ("+this.bewertung+") von '" + this.getTitel() + "' nach "+ bewertung +" geändert.");
        this.bewertung = bewertung;
        
    }

    /**
     * Getter für Bewertung
     *
     * @return Die Bewertung (0.0 bis 10.0)
     */
    public double getBewertung()
    {
        return this.bewertung;
    }

    /**
     * Getter für Titel
     *
     * @return Der Filmtitel
     */
    public String getTitel()
    {
        return this.titel;
    }

}
