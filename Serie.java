import java.time.*;

/**
 * Eine Serie.
 * 
 * @author S.Gebert 
 * @version 12.2020
 */
public class Serie extends Film
{
    /**
     * Die Anzahl Staffeln der Serie
     */
    private int anzahlStaffeln;
    /**
     * Enthält alle Episoden der Serie
     */
    private Filmsammlung episoden;
    /* TODO: Typsicherheit garantieren
     * Nutze statt des Typs Filmsammlung den generischen Typ FilmsammlungGeneric
     *   und schreibe den Code dieser Klasse entsprechend um.
     */

    /**
     * Erzeugt eine Serie
     *
     * @param titel Filmtitel
     * @param produktionsjahr Das Produktionsjahr
     * @param genre Hauptgenre
     * @param altersfreigabe Altersfreigabe in Jahren
     */
    public Serie(String titel, Year produktionsjahr, String genre, int altersfreigabe)
    {
        super(titel, produktionsjahr, genre, -1, altersfreigabe);
        this.typ = FilmTyp.SERIE;
        this.episoden = new Filmsammlung(20);
    }

    /**
     * Gibt die Anzahl aller Episoden der Serie zurück.
     *
     * @return Die Anzahl Episoden der Serie
     */
    public int getAnzahlEpisoden( )
    {
        return this.episoden.anzahlFilme();        
    }

    /**
     * Fügt der Serie eine Episode hinzu.
     *
     * @param episode Die einzufügende Episode
     */
    public void addEpisode( SerienEpisode episode )
    {
        this.episoden.add( episode );
    }
    
    /**
     * Gibt die Episoden der Serie als Filmsammlung zurück
     *
     * @return Die Episoden der Serie als Filmsammlung
     */
    public Filmsammlung getEpisoden()
    {
        /* TODO: Typsicherheit garantieren
         * s.o.
         */
        return this.episoden;
    }

    /**
     * Gibt die Bewertung der Serie zurück.
     * Diese berechnet sich aus dem Mittelwert aller enthaltenen Episoden.
     *
     * @return Die mittlere Bewertung aller Episoden (0.0 bis 10.0)
     */
    @Override
    public double getBewertung( )
    {
        super.setBewertung( this.episoden.getBewertung() );
        return super.getBewertung();
    }

    /**
     * Legt die Bewertung der Serie fest.
     * Da die Bewertung automatisch aus den einzelnen Episoden berechnet wird, wird nichts getan.
     *
     * @param bewertung Bewertung (0.0 bis 10.0)
     */
    @Override
    public void setBewertung( double bewertung )
    {
        if( Settings.VERBOSE == Settings.V_LEVEL.INFO ) System.out.println("Bewertung nicht geändert, da sich die Bewertung aus den Bewertungen der Episoden berechnet.");
        //Nichts tun, da die Bewertung aus der Bewertung der Episoden berechnet wird.
        return;
    }
}
