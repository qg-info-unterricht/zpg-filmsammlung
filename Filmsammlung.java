import java.util.*;

/**
 * Eine Filmsammlung.
 * Einfache Implementierung auf einer Reihung festgelegter Größe.
 * 
 * @author S.Gebert 
 * @version 12.2020
 */
public class Filmsammlung
{

    /**
     * Die der Sammlung zugrundegelegte Reihung. 
     */
    private Film[] sammlung;
    /**
     * Die Anzahl Filme in der Sammlung.
     * Diese muss nicht der Länge der zugrundegelegten Reihung entsprechen. In dieser können sich auch null-Werte befinden.
     */
    private int anzahlFilme = 0;

    /**
     * Erstellt eine leere Filmsammlung.
     *
     * @param laenge Die maximale Anzahl Filme in der Filmsammlung
     */
    public Filmsammlung(int laenge)
    {
        this.sammlung = new Film[laenge];
    }

    /**
     * Erstellt eine Filmsammlung und füllt diese mit gegebenen Filmen.
     *
     * @param sammlung Ein Reihung aller Filme welche in die Filmsammlung aufgenommen werden sollen.
     */
    public Filmsammlung(Film[] sammlung)
    {
        this.sammlung = sammlung;
        this.anzahlFilme = sammlung.length; //Annahme, dass keine Null-Werte in sammlung
    }
    
    /**
     * Erstellt eine Filmsammlung und füllt diese mit gegebenen Filmen.
     *
     * @param sammlung Ein Sammlung aller Filme welche in die Filmsammlung aufgenommen werden sollen.
     */
    public Filmsammlung(Collection<? extends Film> sammlung)
    {
        this.sammlung = sammlung.toArray(new Film[0]);
        this.anzahlFilme = this.sammlung.length; //Annahme, dass keine Null-Werte in sammlung      
    }
    /**
     * Fügt den gegebenen Film an die gegebene Position in der Sammlung ein.
     * Falls sich an der Position bereits ein Film befindet, wird dieser überschrieben. 
     *
     * @param index Die Position in der Sammlung an der der Film eingefügt werden soll
     * @param film Film der in die Sammlung eingefügt werden soll
     */
    public void add( int index, Film film )
    {
        /* TODO: Bounds Check
         * Stelle sicher, dass sich der Wert der Parametervariable index
         *   im zulässigen Bereich befindet.
         */
        sammlung[index] = film;
        /* TODO: Korrekte Filmanzahl
         * Stelle sicher, dass sich die Filmzahl nur dann ändert,
         *   falls an der Position zuvor kein anderer Film war.
         */
        this.anzahlFilme++;
    }

    /**
     * Fügt den gegebenen Film ans Ende der Sammlung ein.
     * Falls kein Platz mehr frei ist, wird der Film nicht in die Sammlung aufgenommen.
     *
     * @param film Film der in die Sammlung eingefügt werden soll
     */
    public void add( Film film )
    {
        for( int i = 0; i < this.sammlung.length; i ++ ){
            if( this.sammlung[i] == null ){
                add(i, film);
                return;
            }
        }
    }

    /**
     * Entfernt einen Film an der angegeben Position aus der Filmsammlung.
     * Die Position bleibt danach leer.
     *
     * @param index Die Position in der Filmsammlung aus der der Film entfernt werden soll
     * @return Der Film, der aus der Sammlung entfernt wurde
     */
    public Film remove( int index )
    {
        /* TODO: Bounds Check
         * Stelle sicher, dass sich der Wert der Parametervariable index
         * im zulässigen Bereich befindet.
         */ 
        Film f = sammlung[index];
        sammlung[index] = null;
        /* TODO: Korrekte Filmanzahl
         * Stelle sicher, dass die Anzahl nur verringert wird,
         *   falls zuvor ein Film an der Position war.
         */
        this.anzahlFilme--;
        return f;        
    }

    /**
     * Gibt den Film an der angegeben Position in der Sammlung zurück
     *
     * @param index Die Position in der Filmsammlung an der sich der zurückzugebende Film befindet
     * @return Der Film an der angegebenen Position
     */
    public Film get( int index )
    {
        /* TODO: Bounds Check
         * Stelle sicher, dass sich der Wert der Parametervariable index
         * im zulässigen Bereich befindet.
         */ 
        return sammlung[index];
    }

    /**
     * Sucht einen Film anhand dessen Titels und gibt die Position in der Sammlung zurück.
     * Konnte der Film nicht gefunden werden, wird -1 zurück gegeben.
     *
     * @param titel Der Titel des gesuchten Films
     * @return Die Position des gesuchten Films
     */
    public int find( String titel )
    {
        /* TODO: Suche
         * Implementiere eine einfache sequentielle bzw. lineare Suche
         */
        return 0; 
    }

    /**
     * Gibt die Anzahl Filme in der Sammlung zurück
     *
     * @return Die Anzahl Filme in der Sammlung
     */
    public int anzahlFilme()
    {
        return this.anzahlFilme;
    }

    /**
     * Bestimmt die durchschnittliche Bewertung aller Filme aus der Sammlung.
     *
     * @return Die durchschnittle Bewertung aller Filme aus der Sammlung
     */
    public double getBewertung( )
    {
        double sum = 0.0;
        int num = 0;     

        for( int i = 0; i < this.sammlung.length; i++ ){
            if( this.sammlung[i] != null ){
                sum += this.sammlung[i].getBewertung();
                num ++;
            }
        }

        return Math.round(sum/num * 10.0) / 10.0; //auf eine Nachkommastelle runden.
    }
}
